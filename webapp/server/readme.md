# Server

A simple Express server.  
Will use [mongoose.js](http://mongoosejs.com/) for communication with a NoSQL database ([MongoDB](https://www.mongodb.com/)).

## Structure

```
server
|-- bin
|   `-- www             //Executable
|-- app.ts              //App
`-- tsconfig.json       //Compiler options
```