# Playlstr - Front-end

## What is this?

This is mostly the front-end.

## Libraries

- [Angular](https://angular.io/)
- [Express](http://expressjs.com/)
- [SystemJS](https://github.com/systemjs/systemjs)
- [gulp](http://gulpjs.com/)
- [angular2-mdl](https://www.npmjs.com/package/angular2-mdl)
- [angular2-notifications](https://www.npmjs.com/package/angular2-notifications)
- [angular-tag-cloud-module](https://www.npmjs.com/package/angular-tag-cloud-module)

## Documentation

### Architecture

![Arch](https://dl.dropboxusercontent.com/u/41553341/arch2.png)

### Structure

+ /
    + **[client/](./client/)** Code for the client.   
    + **[public/](./public/)** Production folder (client).  
    + **[server/](./server/)** Code for the server.  
    + **[tools/](./tools/)** Tools and configurations for development and production.  
    + **[gulpfile.js](./gulpfile.js)** Configuration file for gulp.  
    + **[nodemon.json](./nodemon.json)** Configuration file for nodemon.  
    + **[package.json](./package.json)** Metadata for the project.  

## Install
```bash
git clone https://{{user}}@bitbucket.org/trondaal/it2810-18-oppgave-3.git
cd it2810-18-oppgave3/webapp

# Install dependencies
npm install

# Application url: http://localhost:3000
```

## Development
Uncomment in public/index.html:
```html
<!-- Development mod -->
<script src="assets/js/systemjs.config.js"></script>
<script>
    System.import('app').catch(function(err) { console.error(err); });
</script>
```

Comment out
```html
<!-- Production mod -->
<script src="js/bundle.min.js"></script>
```

Start server:
```bash
npm run develop
```

## Production

Uncomment in public/index.html:
```html
<!-- Production mod -->
<script src="js/bundle.min.js"></script>
```

Comment out
```html
<!-- Development mod -->
<script src="assets/js/systemjs.config.js"></script>
<script>
    System.import('app').catch(function(err) { console.error(err); });
</script>
```

Build:
```bash
npm run build:client
```
This will bundle the application to *public/assets/js/bundle.min.js* and move the templates to *public/client/*.

Start server:
```bash
node server/www
```