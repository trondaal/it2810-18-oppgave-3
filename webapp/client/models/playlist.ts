export class Playlist {
    name: string;
    spotifyURI: string;
    tags: string[];
    creator: string;
  }
