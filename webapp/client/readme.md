# Client

This is just a protoype and is subject to change.

## Structure

+ **client**
    + **[actions/](./actions/)**
    Payloads of information that send data from the application to the store (redux).
    + **[components/](./components)**
    Building blocks for the application. Controls a portion of the screen.
    + **[config/](./config)**
    Contains configuration options for the application.
    + **[directives/](./directives)**
    Custom directives for the application.
    + **[models/](./models)**
    Objects.
    + **[pipes/](./pipes)**
    Custom pipes. Takes data as input and transforms it to a desired output.
    + **[reducers/](./reducers)**
    Specify how the application's state changes in response to actions (redux).
    + **[service/](./service)**
    Shared functionality between components.
    + **[app.component.ts](./app.component.ts)**
    The root component for the application.
    + **[app.module.ts](./app.module.ts)**
    The root module for the application.
    + **[main.ts](./main.ts)**
    Initializes the application.
    + **[routes.ts](./routes.ts)**
    Routing.
    + **[tsconfig.json](./tsconfig.json)**
    Compiler options.
