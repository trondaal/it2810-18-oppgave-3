import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MdlModule} from 'angular2-mdl'


import { AppComponent } from "./app.component";
import { PlaylistService } from "./service/playlist.service";
import {routing} from './routes';
import {DashboardModule} from "./modules/dashboard/dashboard.module";
import {BrowseModule} from "./modules/browse/browse.module";
import {PlaylistsModule} from "./modules/playlists/playlists.module";
import {RegisterModule} from "./modules/register/register.module";
import {RegisterService} from "./service/register.service";
import {UploadModule} from "./modules/upload/upload.module";
import {HttpModule} from "@angular/http";
import {AuthService} from "./service/auth.service";
import {FormsModule} from "@angular/forms";
import {TagCloudModule} from "angular-tag-cloud-module";
import {LoginModule} from "./modules/login/login.module";
import {MyPageModule} from "./modules/myPage/myPage.module";


@NgModule({
    imports: [
        BrowserModule,
        DashboardModule,
        BrowseModule,
        PlaylistsModule,
        RegisterModule,
        UploadModule,
        LoginModule,
        routing,
        HttpModule,
        MdlModule,
        FormsModule,
        TagCloudModule,
        MyPageModule
        // MaterialModule.forRoot()
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        PlaylistService,
        RegisterService,
        AuthService
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
