import {Headers, Http} from '@angular/http';
import {Injectable, Inject} from '@angular/core';
import {Config} from "../config/config";

import 'rxjs/add/operator/map';

/**
 * Provides methods for registration.
 */
@Injectable()
export class RegisterService {

	constructor(@Inject(Http) private http: Http) {

	}

	registerUser(username: string, pass: string) {
		var body = {
			username: username,
			password: pass,
		};

		this.http
			.post(Config.URL_API_USERS, body)
			.map(response => response.json())
			.subscribe(
				response => console.log(response)
			)
	}
}
