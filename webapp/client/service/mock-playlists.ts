import {Playlist} from '../models/playlist';

export const PLAYLISTS: Playlist[] = [
    {name: 'Spilleliste 1', spotifyURI: '', tags: ['#chill', '#gløs', '#arne', '#bjarne?'], creator: 'arne'},
    {name: 'Pop 2016', spotifyURI: '', tags: ['#pop', '#2016', '#radio'], creator: 'bjarne'},
    {name: 'VG lista', spotifyURI: '', tags: ['#vg', '#2016'], creator: 'arne'},
    {name: 'Jul', spotifyURI: '', tags: ['#ferie', '#jul', '#snø', '#kaker'], creator: 'bjarnearne'},
    {name: 'Kake', spotifyURI: '', tags: ['#house'], creator: 'kake'},
    {name: 'sl6', spotifyURI: '', tags: ['#dnb'], creator: 'sjokolade'}
];
