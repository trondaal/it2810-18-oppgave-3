import {Injectable, Inject} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Playlist} from "../models/playlist";
import {Config} from "../config/config";

/**
 * Provides methods for executing search to the API.
 */
@Injectable()
export class PlaylistService {

    constructor(@Inject(Http) private http: Http) {

    }

    getPlaylistsWithToken(token) {
        return this.http.get(Config.URL_API_PLAYLISTS + `?${token}`,{withCredentials: true})
            .map(this.extractData).toPromise();
    }

    searchPlaylists(token): Promise<Playlist[]> {
        return this.http.get(Config.URL_API_PLAYLISTS + `?search=${token}`,{withCredentials: true})
            .map(this.extractData).toPromise();
    }

    searchPlaylistsByTag(token): Promise<Playlist[]> {
        return this.http.get(Config.URL_API_PLAYLISTS + `?tag=${token}`,{withCredentials: true})

            .map(this.extractData).toPromise();
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || [];
    }

    getPlaylists(callback){
      $.ajax({
            method: 'GET',
            url: Config.URL_API_PLAYLISTS,
            json: true,
            success: function(data){
              console.log(data);
              callback(data);
            },
            error: function(data){
              console.log(data);
              callback([]);
            }
        });
    }
}
