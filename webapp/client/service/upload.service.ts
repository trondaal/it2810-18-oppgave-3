import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import {Config} from "../config/config";

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/**
 * Provides methods for uploading a playlist to the database
 * via the API
 */
@Injectable()
export class UploadService {

  constructor(private http: Http) { }

  uploadPlaylist(data) {
    var endpoint = Config.URL_API_PLAYLISTS;
    var body = {
        name: data.name,
        spotifyURI: data.spotifyURI,
        tags: data.tags
    };

    return this.http
      .post(endpoint, body, {withCredentials: true})
      .map(res => res.json())

  }

  login(){
    var endpoint = Config.URL_API_LOGIN;
    var params = ({username: "LarsMoe", password: "test123"});
    var headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    console.log(params);

    return this.http.post(endpoint, params)
    .map(res => res.json())

  }

}
