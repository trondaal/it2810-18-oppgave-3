import { Injectable }    from '@angular/core';
import {Http, Response} from "@angular/http";
import {AuthService} from "../service/auth.service";

import 'rxjs/add/operator/map';

@Injectable()
export class MyPageService {



  constructor(private http: Http,
              private authService: AuthService) { }
private username = this.authService.getUsername();

getUserID(){
  return this.http
    .get("http://localhost:8080/api/users/me")
    .map(this.extractData).toPromise();

}


getSearches(){
  return this.http
    .get("http://localhost:8080/api/search")
    .map(this.extractData).toPromise();

}

private extractData(res: Response) {
    let body = res.json();
    return body || [];
}

filterQuery(response){
  var querys = []
  var length = response.recentSearches.length;
  querys.push(response.recentSearches[0].query);
  querys.push(response.recentSearches[1].query);
  querys.push(response.recentSearches[2].query);


  return querys;
}

}
