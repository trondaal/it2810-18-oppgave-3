import {Http} from "@angular/http";
import {Injectable, Inject} from "@angular/core";
import "rxjs/add/operator/map";
import {Config} from "../config/config";

/**
 * Provides methods for authentication
 */
@Injectable()
export class AuthService {

    private username: string = '';
    private password: string = '';

    constructor(@Inject(Http) private http: Http) {

    }
    getUsername(){
      return this.username;
    }

    post(url, body) {
        var url = Config.URL_API + url;
        return this.http.post(url, body, {withCredentials: true});
    }

    logout() {
        var body = {
            username: "potao"
        };

        this.post("logout", body).subscribe(
            response => console.log(response)
        )
    }

    public setUser(username: string, pass: string){
        this.username = username;
        this.password = pass;
    }

    login(username: string, pass: string) {
        var body = {
            username: username,
            password: pass,
        };

        return this.post("login", body);
    }

    checkCredentials() {
        this.login(this.username, this.password).subscribe(res => {
            return (res.status == 200);
        });
    }

}
