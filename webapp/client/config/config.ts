
export class Config {
    //public static URL_API: string = "http://localhost:8080/api/";
    public static URL_API: string = "http://it2810-18.idi.ntnu.no:8080/api/";

    public static URL_API_PLAYLISTS = Config.URL_API + 'playlists';
    public static URL_API_USERS = Config.URL_API + 'users';
    public static URL_API_LOGIN = Config.URL_API + 'login';

}
