import {RouterModule, Routes} from '@angular/router';

import {MyPageComponent} from "../../components/myPage/myPage.component";

export const routes: Routes = [
    {path: 'myPage', component: MyPageComponent}
];

export const routing = RouterModule.forChild(routes);