import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./myPage.routing";
import {MyPageComponent} from "../../components/myPage/myPage.component";

@NgModule({
    imports: [
        CommonModule,
        routing,
    ],
    declarations: [
        MyPageComponent
    ],
    bootstrap: [
        MyPageComponent
    ],
})
export class MyPageModule {}