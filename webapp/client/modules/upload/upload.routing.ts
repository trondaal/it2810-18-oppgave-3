import {RouterModule, Routes} from '@angular/router';

import {UploadComponent} from "../../components/upload/upload.component";

export const routes: Routes = [
    {path: 'upload', component: UploadComponent}
];

export const routing = RouterModule.forChild(routes);
