import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./upload.routing";
import {UploadComponent} from "../../components/upload/upload.component";
import {TagInputModule } from 'ng2-tag-input';
import { FormsModule } from '@angular/forms';
import { SimpleNotificationsModule, PushNotificationsModule} from 'angular2-notifications';



@NgModule({
    imports: [
        CommonModule,
        routing,
        TagInputModule,
        FormsModule,
        SimpleNotificationsModule,
        PushNotificationsModule

    ],
    declarations: [
        UploadComponent
    ],
    bootstrap: [
        UploadComponent
    ],
    schemas: [
      CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class UploadModule {}
