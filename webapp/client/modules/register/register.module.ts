import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from "./register.routing";

import { RegisterComponent }   from '../../components/register/register.component';

@NgModule({
  imports:      [ 
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  routing
  ],
  declarations: [ 
  RegisterComponent
  ],
  bootstrap:    [ RegisterComponent ]
})
export class RegisterModule { }
