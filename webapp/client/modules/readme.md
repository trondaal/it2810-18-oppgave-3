# Modules

## Structure

```
modules
`-- X
    |-- X.module.ts
    `-- X.routing.ts
```

## Documentation

### Browse

Module for a "browse" view. Contains one component; [*browse*](../components/browse/browse.component.ts).

### Dashboard

Module for a placeholder dashboard. Contains one component; [*dashboard*](../components/dashboard/dashboard.component.ts).

### Playlists

Module for the playlists view. Contains to components; [*playlists*](../components/playlists/playlists.component.ts) and [*playlist-detail*](../components/playlists/playlist-detail/playlist-detail.component.ts).

### Shared
Shared module - Placeholder