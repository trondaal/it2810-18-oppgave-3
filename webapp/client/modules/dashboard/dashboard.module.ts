import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./dashboard.routing";
import {FormsModule} from "@angular/forms";
import {DashboardComponent} from "../../components/dashboard/dashboard.component";
import {MdlModule} from 'angular2-mdl'
import {TagCloudModule} from "angular-tag-cloud-module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        routing,
        MdlModule,
        TagCloudModule
    ],
    declarations: [
        DashboardComponent
    ],
    bootstrap: [
        DashboardComponent
    ]
})
export class DashboardModule {}