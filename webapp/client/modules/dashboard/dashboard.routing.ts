import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from '../../components/dashboard/dashboard.component';

export const routes: Routes = [
    {path: 'dashboard', component: DashboardComponent}
];

export const routing = RouterModule.forChild(routes);