import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./login.routing";
import {FormsModule} from "@angular/forms";
import {MdlModule} from 'angular2-mdl'
import {TagCloudModule} from "angular-tag-cloud-module";
import {LoginComponent} from "../../components/login/login.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        routing,
        MdlModule,
        TagCloudModule
    ],
    declarations: [
        LoginComponent
    ],
    bootstrap: [
        LoginComponent
    ]
})
export class LoginModule {}