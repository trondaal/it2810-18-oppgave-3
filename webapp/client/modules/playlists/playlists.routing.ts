import {RouterModule, Routes} from '@angular/router';

import {PlaylistsComponent} from "../../components/playlists/playlists.component";

export const routes: Routes = [
    {path: 'playlists', component: PlaylistsComponent}
];

export const routing = RouterModule.forChild(routes);