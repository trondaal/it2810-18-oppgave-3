import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./playlists.routing";
import {PlaylistsComponent} from "../../components/playlists/playlists.component";
import {PlaylistDetailComponent} from "../../components/playlists/playlist-detail/playlist-detail.component";
import {MdlModule} from "angular2-mdl";
import {FormsModule} from "@angular/forms";
import {TagInputModule } from 'ng2-tag-input';


@NgModule({
    imports: [
        CommonModule,
        routing,
        MdlModule,
        FormsModule,
        TagInputModule
    ],
    declarations: [
        PlaylistsComponent,
        PlaylistDetailComponent
    ],
    bootstrap: [
        PlaylistsComponent
    ],
})
export class PlaylistsModule {}
