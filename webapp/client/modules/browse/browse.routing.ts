import {RouterModule, Routes} from '@angular/router';

import {BrowseComponent} from "../../components/browse/browse.component";

export const routes: Routes = [
    {path: 'browse', component: BrowseComponent}
];

export const routing = RouterModule.forChild(routes);