import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {routing} from "./browse.routing";
import {BrowseComponent} from "../../components/browse/browse.component";
import {MdlModule} from "angular2-mdl";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        routing,
        MdlModule,
        FormsModule
    ],
    declarations: [
        BrowseComponent
    ],
    bootstrap: [
        BrowseComponent
    ],
})
export class BrowseModule {}