import {Component,OnInit} from '@angular/core';
import { UploadService } from '../../service/upload.service';
import {NotificationsService} from 'angular2-notifications';




@Component({
    selector: 'upload',
    styleUrls: ['client/components/upload/upload.component.css'],
    templateUrl: 'client/components/upload/upload.component.html',
    providers: [UploadService]



})

export class UploadComponent {

    constructor(private uploadService:UploadService, private _service: NotificationsService) {}


    playlist = {
      name: '',
      spotifyURI: '',
      tags: []
    }

    title = "Success!";
    autocompleteItems=['Pop', 'Rock', 'Chillout','Indie',"Drum'n'bass",'Trance','Techno','Folk','80s'
  ,'90s','Metal','Black Metal']

  public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: true,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: 'visible',
        rtl: false,
        animate: 'scale',
        position: ['right', 'bottom']
    };

    public type: string = 'success';
    public content: string = 'Playlist uploaded!';

    onSubmit() {
      console.log("Clicked");
      this.uploadService.uploadPlaylist(this.playlist)
          .subscribe(
            data => console.log(data),
            error => alert(error),
            () => this.create()
          );
          this.clearForm();

    }

    clearForm() {
      this.playlist.name = '';
      this.playlist.spotifyURI = '';
      this.playlist.tags = [];
    }

    onCreate(event) {
        console.log(event);
    }

    onDestroy(event) {
       console.log(event);
   }

   create() {
        switch (this.type) {
            case 'success':
                let a = this._service.success(this.title, this.content, {id: 123});
                break;
            case 'alert':
                this._service.alert(this.title, this.content);
                break;
            case 'error':
                this._service.error('Failed to upload playlist', this.content);
                break;
            case 'info':
                this._service.info(this.title, this.content);
                break;
            case 'bare':
                this._service.bare(this.title, this.content);
                break;
        }
    }

  }
