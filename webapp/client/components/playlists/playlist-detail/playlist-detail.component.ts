import {Component, Input} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Playlist} from '../../../models/playlist';



@Component({
    selector: 'playlist-detail',
    styleUrls: ['client/components/playlists/playlist-detail/playlist-detail.component.css'],
    templateUrl: 'client/components/playlists/playlist-detail/playlist-detail.component.html'
})


export class PlaylistDetailComponent {
   @Input()
   playlist: Playlist;

   widget:SafeResourceUrl;

   constructor(private domSanitizer : DomSanitizer) {}

   ngOnChanges(){
     if (this.playlist) {
       this.widget = this.domSanitizer.bypassSecurityTrustResourceUrl('https://embed.spotify.com/?uri=' + this.playlist.spotifyURI);
       console.log(this.widget);
     }
   }



}
