import {Component, OnInit} from '@angular/core';
import {Playlist} from '../../models/playlist';
import {PlaylistService} from '../../service/playlist.service';



@Component({
    selector: 'playlists',
    styleUrls: ['client/components/playlists/playlists.component.css'],
    templateUrl: 'client/components/playlists/playlists.component.html',
    providers: [PlaylistService],
})
export class PlaylistsComponent implements OnInit {
    title = "Playlstr";
    selectedPlaylist: Playlist;
    playlists: Playlist[];
    constructor(private playlistService: PlaylistService) {}

    getPlaylists(): void {
        this.playlistService.getPlaylists(this.updatePlaylists.bind(this));
    }

    updatePlaylists(playlists): void {
      this.playlists = playlists;
    }

    ngOnInit(): void {
        this.getPlaylists();
    }

    onSelect(playlist: Playlist): void {
        this.selectedPlaylist = playlist;

    }



}
