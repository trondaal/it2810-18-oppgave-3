import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import {RegisterService} from "../../service/register.service";

@Component({
  selector: 'my-app',
  styleUrls: ['client/components/register/register.css'],
  template: 
  `
  <div id="regForm">
    <h1>(License and) Registration please!</h1>
    <form [formGroup]="registerForm" (ngSubmit)="onSubmit()">
      <label>Username: </label>
      <input type="text" formControlName="username" required>
      <br>
      <br>
      <label>Password: </label>
      <input type="text" formControlName="password" required>
      <br>
      <br>
      <button type="submit">Registrate user</button>
    </form>
    </div>
  `
})

export class RegisterComponent{
/*
  dude = {
    username: "ro",
    password: "secret"
  }

  onSubmit(){
    console.log(this.dude.username);
    console.log(this.dude.password);
  }*/


  registerForm: FormGroup;

  username = new FormControl("", Validators.required);

  constructor(formBuilder: FormBuilder, private registerService:RegisterService) {
    this.registerForm = formBuilder.group({
      "username": this.username,
      "password":["", Validators.required]
    });
  } 

  onSubmit(){
    if(this.registerForm !== undefined){
      /*heyname = this.registerForm;*/
      /*heypass = this.registerForm._value.password;*/
      
      console.log(`navn: ${this.registerForm.controls.username.value}`);
      console.log(`password: ${this.registerForm.controls.password.value}`);
      /*console.log(heyname);
      console.log(this.registerForm._value.password);*/
      this.registerService.registerUser(this.registerForm._value.username, this.registerForm._value.password);

    }}

}
