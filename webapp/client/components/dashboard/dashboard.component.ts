import { Component } from '@angular/core';
import {User} from "../../models/user";
import {AuthService} from "../../service/auth.service";
import {Playlist} from "../../models/playlist";
import {PlaylistService} from "../../service/playlist.service";

@Component({
    selector: 'dashboard',
    styleUrls: ['client/components/dashboard/dashboard.component.css'],
    templateUrl: 'client/components/dashboard/dashboard.component.html',
})

export class DashboardComponent {

    public user = new User('', '');
    public error = '';
    public data: { [id: string]: number} = {};
    public cloudData = [];
    public dataAvailable = false;
    private playlists: Playlist[] = [];
    private skipFactor: number = 0;

    constructor(
        private authService: AuthService,
        private playlistService: PlaylistService
    ) {}

    ngOnInit() {
        this.getPlaylists();
    }

    /**
     * Sends a request to PlaylistService and parses the data
     * to a valid format for <angular-tag-cloud>
     */
    private getPlaylists() {
        var token: string = `skip=${this.skipFactor * 20}`;

        this.playlistService.getPlaylistsWithToken(token).then(
            playlists =>  {
                this.playlists = this.playlists.concat(<Playlist[]>playlists);
                if (playlists.length > 19) {
                    this.skipFactor += 1;
                    this.getPlaylists()
                } else {
                    this.skipFactor = 0;
                    for (var playlist in this.playlists) {
                        var tempPl: Playlist = this.playlists[playlist];

                        for (var t in tempPl.tags) {
                            var tag: string = tempPl.tags[t];
                            if (this.data[tag]) {
                                this.data[tag] += 1;
                            } else {
                                this.data[tag] = 1;
                            }
                        }
                    }
                    for (var tag in this.data) {
                        this.cloudData.push( {text: tag, weight: this.data[tag]} )
                    }
                    this.dataAvailable = true;
                }
            }
        );
    }

}