import { Component } from '@angular/core';
import {User} from "../../models/user";
import {AuthService} from "../../service/auth.service";
import {MyPageService} from "../../service/myPage.service";

@Component({
  selector: 'myPage',
  styleUrls: ['client/components/myPage/myPage.component.css'],
  templateUrl: 'client/components/myPage/myPage.component.html',
  providers: [MyPageService, AuthService],
})
export class MyPageComponent {
  constructor(
      private authService: AuthService,
      private myPageService: MyPageService

  ) {}
  username = this.authService.getUsername();
  playlists = ["one", "two", "three"]
  recentSearches = ["party", "jazz", "pop"]
  searches = this.myPageService.getSearches();


  logout() {
      this.authService.logout()
  }


jsonobj =
{
 "_id": "5828c48db961bc2bff709694",
 "username_lower": "petter",
 "username": "Petter",
 "password": "$2a$10$Sfh41OEx.O/Q7ymkduqYDOUjFVNcmt.SDWEiWiaF6367mY14creAC",
 "__v": 10,
 "recentSearches": [
   {
     "_id": "5828d71af33827304a124777",
     "user": "5828c48db961bc2bff709694",
     "query": "",
     "__v": 0,
     "tags": []
   },
   {
     "_id": "5828d712f33827304a124776",
     "user": "5828c48db961bc2bff709694",
     "query": "petter",
     "__v": 0,
     "tags": []
   },
   {
     "_id": "5828d711f33827304a124775",
     "user": "5828c48db961bc2bff709694",
     "query": "pette",
     "__v": 0,
     "tags": []
   },
   {
     "_id": "5828d711f33827304a124774",
     "user": "5828c48db961bc2bff709694",
     "query": "pett",
     "__v": 0,
     "tags": []
   }
 ],
 "playlists": [
   {
     "_id": "5828e1a3b9d0b73281e29183",
     "creator": "5828c48db961bc2bff709694",
     "name_lower": "rock",
     "name": "Rock",
     "spotifyURI": "vibe",
     "__v": 0,
     "tags_lower": [
       "chill"
     ],
     "tags": [
       "CHILL"
     ]
   },
   {
     "_id": "5828e1aeb9d0b73281e29184",
     "creator": "5828c48db961bc2bff709694",
     "name_lower": "testlist",
     "name": "Testlist",
     "spotifyURI": "vibe",
     "__v": 0,
     "tags_lower": [
       "indie"
     ],
     "tags": [
       "indie"
     ]
   }
 ]
}





}
