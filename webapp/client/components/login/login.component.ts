import { Component } from '@angular/core';
import {User} from "../../models/user";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'login',
    styleUrls: ['client/components/login/login.component.css'],
    templateUrl: 'client/components/login/login.component.html',
})

export class LoginComponent {

    public user = new User('', '');
    public error: string = '';

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    logout() {
        this.authService.logout()
    }

    login() {
        this.authService.login(this.user.username, this.user.password)
            .subscribe(response => {
            if (response.status == 200) {
                this.authService.setUser(this.user.username, this.user.password);
                this.router.navigateByUrl("dashboard");
            } else {
                this.error = "Ops, something bad happend :3";
            }
            }, error => {
                if (error.status == 422){
                    this.error = "Missing username or password!";
                } else if (error.status == 404) {
                    this.error = "Wrong username or password!";
                } else {
                    this.error = "Ops, something bad happend :3";
                }
        })
    }

    /*createTestUser() {
        this.authService.createTestUser();
    }

    testSpillelistePost() {
        this.authService.testSpillelistePost();
    }*/


}