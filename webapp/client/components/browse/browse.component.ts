import {Component} from '@angular/core';
import {AuthService} from "../../service/auth.service";
import {Playlist} from "../../models/playlist";
import {PlaylistService} from "../../service/playlist.service";
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
    selector: 'browse',
    styleUrls: ['client/components/browse/browse.component.css'],
    templateUrl: 'client/components/browse/browse.component.html'


})

export class BrowseComponent {

    public search = "";
    public playlists: Playlist[] = [];
    public title = "Playlstr";
    public radioOption: string = "name";
    private skipFactor: number = 0;

    constructor(
        private authService: AuthService,
        private playlistService: PlaylistService,
        private domSanitizer : DomSanitizer
    ) {}


    /**
     * Starts the search procedure.
     */
    startSearch(): void{
        this.playlists = [];
        this.searchPlaylists();
    }

    /**
     * Sends multiple request to PlaylistService and appends
     * the results to this.playlists.
     */
    searchPlaylists() {
        var search = this.search + `&skip=${this.skipFactor * 20}`;

        if (this.radioOption == "name") {
            this.playlistService.searchPlaylists(search).then(
                playlists =>  {
                    this.playlists = this.playlists.concat(<Playlist[]>playlists);
                    if (playlists.length > 19) {
                        this.skipFactor += 1;
                        this.searchPlaylists()
                    } else {
                        this.skipFactor = 0;
                    }
                }
            );
        } else {
            this.playlistService.searchPlaylistsByTag(search).then(
                playlists =>  {
                    this.playlists = this.playlists.concat(<Playlist[]>playlists);
                    if (playlists.length > 19) {
                        this.skipFactor += 1;
                        this.searchPlaylists()
                    } else {
                        this.skipFactor = 0;
                    }
                }
            )
        }
    }

    redirect(uri){
       window.location.href=uri;
       console.log("clicked!");
       console.log(uri);
    }





}
