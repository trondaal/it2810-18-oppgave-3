# Components

## Structure

```
components
`-- X
    |-- X.component.ts
    |-- X.component.html
    |-- X.component.css
    `-- childY
        |-- Y.component.ts
        `-- ...
```
