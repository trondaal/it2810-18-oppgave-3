import {Component} from "@angular/core";
import {AuthService} from "./service/auth.service";

@Component({
    selector: 'app',
    styleUrls: ['client/app.component.css'],
    templateUrl: 'client/app.component.html'
})

export class AppComponent {
    title = 'PLAYLSTR';

    constructor(
        private authService: AuthService
    ) {}

    ngOnInit() {

    }

}
