var gulp = require('gulp');
var path = require('path');
var Builder = require('systemjs-builder');
var ts = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

var tsProject = ts.createProject('./client/tsconfig.json');

var appDev = './client';
var appProd = './public';

//Compiles typescript.
gulp.task('ts', function() {
    return gulp.src(appDev + '/**/*.ts')
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(ts(tsProject))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(appDev))
});

//Copy html templates
gulp.task('html', function () {
    return gulp.src(appDev + '/**/*.html')
        .pipe(gulp.dest(appProd + '/client'));
});

//Copy css files
gulp.task('css', function () {
    return gulp.src(appDev + '/**/*.css')
        .pipe(gulp.dest(appProd + '/client'));
});

//Bundles compiled code and minifies it.
gulp.task('bundle', ['ts'], function() {
    var builder = new Builder('', './tools/systemjs.config.js');

    return builder
        .buildStatic('app', appProd + '/assets/js/bundle.min.js', {minify: true, mangle: true, rollup: true})
        .then(function() {
            console.log('Build complete');
        })
        .catch(function (err) {
            console.log('Build error!');
            console.log(err);
        })
});

gulp.task('build:client', ['ts', 'html', 'css', 'bundle']);