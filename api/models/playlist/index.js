var mongoose = require('mongoose');

var playlistSchema = new mongoose.Schema({
	name: {type: String, required: true},
	name_lower: {type: String, required: true}, //store lowercase for case insensitive search (regexp is slow and should be avoided)
	spotifyURI: {type: String, required: true},
	tags: {type: [String]},
	tags_lower: {type: [String]},
	created_at: {type: Date, default: Date.now()},
  creator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Playlist', playlistSchema);
