var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Promise = require('bluebird');
Promise.promisifyAll(bcrypt);

var userSchema = new mongoose.Schema({
	username: {type: String, required: true},
	username_lower: {type: String, required: true}, //store lowercase for case insensitive search (regexp is slow and should be avoided)
	password: {type: String, required: true},
	playlists: [{type: mongoose.Schema.Types.ObjectId, ref: 'Playlist'}],
  recentSearches: [{type: mongoose.Schema.Types.ObjectId, ref: 'Search'}]
});

userSchema.methods.validPassword = function (password) {
	return bcrypt
	.compareAsync(password, this.password)
	.then(function (result, err) {
		if (err) throw new Error(err);
		return result;
	});
};

module.exports = mongoose.model('User', userSchema);
module.exports.hashPassword = function(password){
	return bcrypt.hashSync(password, 10);
}
