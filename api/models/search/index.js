var mongoose = require('mongoose');

var searchSchema = new mongoose.Schema({
	tags: {type: [String], required: false},
	query: {type: String, required: false},
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Search', searchSchema);
