# Playlstr - API

## What is this?

The API is responsible for communication between the database and the front-end application.

## Libraries

- [Node.js](https://nodejs.org/en/)
- [Express](http://expressjs.com/)
- [SystemJS](https://github.com/systemjs/systemjs)
- [mongoose.js](http://mongoosejs.com/)

## Documentation

### Architecture

![Arch](https://dl.dropboxusercontent.com/u/41553341/arch2.png)

### Data models

+ User
    + **Username** String
    + **Password** String
    + **Playlists** Playlist[]
    + **RecentSearches** Search[]
+ Playlist
    + **Name** String
    + **SpotifyURI** String
    + **Tags** String[]
    + **Creator** User
+ Search
    + **Tag** String[]
    + **User** User[]
    + **Query** String

### Structure

+ /
    + **[config/](./config/)** Configuration files for the server.   
    + **[models/](./models/)** Schemas for the database.  
    + **[routes/](./routes/)** Code for the server.  
    + **[package.json](./package.json)** Metadata for the project.  
    + **[server.js](./server.js)** Server executable.  

## Install
```bash
git clone https://{{user}}@bitbucket.org/trondaal/it2810-18-oppgave-3.git
cd it2810-18-oppgave3/api

# Install dependencies
npm install

# Application url: http://localhost:8080/api
```

## Running
Start the server
```bash
node server.js
```
