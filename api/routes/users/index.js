var express = require('express');
var router = express.Router();
var User = require('../../models/user');
var validation = require('./validateRequests');
var authenticate = require('../login/authenticate');
module.exports = function () {
  router.post(
    '/users',
    validation.userPost,
    saveUser
  );

  router.get(
    '/users',
    function(req, res){
      User.find({})
      .populate('recentSearches')
      .select('-password')
      .exec(function (err, users){
        res.status(200).json(users);
      });
    }
  );

  router.get(
    '/users/me',
    authenticate.normal,
    function(req, res){
      User.findOne({_id: req.session.user._id})
      .populate('playlists')
      .populate('recentSearches')
      .exec(function(err, user){
        if(err) return res.status(500).send();
        if(!user) return res.status(404).json({message: 'user with given id not found'});
        return res.status(200).json(user);
      });
    }
  );

  return router;
};

function saveUser(req, res) {
  var newUser = new User(req.body);
  newUser.username_lower = newUser.username.toLowerCase();
  newUser.password = User.hashPassword(newUser.password);
  User.findOne({username_lower: newUser.username_lower}, function(err, user){
    if(err) { //Unexpected error
      console.log(err);
      res.status(500).send();
    }
    if (user) { //Another user with given username exists
      return res.status(409).send('Username already in use');
    }
    else{
      newUser.save(function(err, newUser){
        if (err) {
          console.log(err);
          return res.status(500).send();
        }
        return res.status(200).json(newUser);
      });
    }
  });
}
