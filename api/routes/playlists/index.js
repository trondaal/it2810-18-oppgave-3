var express = require('express');
var router = express.Router();
var Playlist = require('../../models/playlist');
var Search = require('../../models/search');
var User = require('../../models/user');
var playlistValidation = require('./validation.js');
var validateSession = require('../../routes/login/authenticate');
var config = require('../../config/search.js');

module.exports = function () {
  router.post('/playlists',
    validateSession.normal,
    playlistValidation.playlistPost, //returns malformed entity exception if the required fields are not sent with the request
    storePlaylist
  );

  router.get('/playlists',
    playlistValidation.playlistGet,
    populateOwnerIds,
    storeSearch,
    executePlaylistSearch
  );

  return router;
};
/**
 * Creates a playlist, parsed from the request body and stores it to the db.
 * Returns the created playlist as json to the request.
 */
function storePlaylist(req, res) {
  var playlist = new Playlist(req.body);
  playlist.name_lower = playlist.name.toLowerCase();
  playlist.tags_lower = playlist.tags.map(tag => {
    return tag.toLowerCase();
  });
  playlist.creator = req.session.user._id;
  User.findOne({_id: req.session.user._id}, function(err, user){
    if(err) return res.status(500).send();
    user.playlists.push(playlist._id);
    user.save(function(err, user){
      playlist.save(function(err, playlist){
        if (err) {
          console.log(err);
          return res.status(500).send();
        }
        res.status(200).json(playlist);
      });
    })
  });
}

/**
 * Middleware for GET /playlists
 * Maps the search query to a list of user ids so that playlists related to the
 * given user can be found later. Adds the user ids found to req.query.userIds.
 */
function populateOwnerIds(req, res, next) {
  if(!req.query.search || !req.query.search.length === 0){
      return next();
  }
  var ownerNameSearch = req.query.search.toLowerCase(); //case insensitive

  User.find()
      .where({username_lower: new RegExp('^'+ownerNameSearch)}) //starts with search
      .limit(config.DEFAULT_QUERY_LIMIT) //limit the search to reduce processing time should the user base grow...
      .exec(function (err, users) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        if(!users){
          req.query.userids = [];
          next();
        }
        else {
          req.query.userIds = [];
          users.forEach(function(item, index){
              req.query.userIds.push(item._id);
          });
        }
        next();
  });
}

/**
 * Processed query parameters:
 * search: a normal search query, will be used to search in usernames and tags
 * tag: one tag entry must be present for each tag to search for, e.g. /playlist?tag=party&tag=rock
 * skip: used for pagination, must be present if the desire is to load the next X playlists,
 * /playlists?search=rock => /playlists?search=rock&skip=X => /playlists?search=rock&skip=2*X
 */
function executePlaylistSearch(req, res) {
  var skip = req.query.skip ? parseInt(req.query.skip) : 0; //pagination, defult to 0
  var dbQuery = {$or: []};
  if(req.query.search && req.query.search.length !== 0){
    if(!req.query.tag) req.query.tag = [];
    req.query.tag.push(req.query.search); //also search in tags for query
    dbQuery.$or.push({name: new RegExp('^'+req.query.search.toLowerCase())}); //case insensitive, starts with search
    dbQuery.$or.push({creator: {$in: req.query.userIds}}); //tags contains specified tags or query
  };
  if(req.query.tag && req.query.tag.length !== 0){
    if(typeof req.query.tag === 'string'){
      dbQuery.$or.push({tags_lower: new RegExp('^'+req.query.tag.toLowerCase())});
    }else{
      var tags_lower = req.query.tag.map(tag => {
        return tag.toLowerCase();
      });
      dbQuery.$or.push({tags_lower: {$in: tags_lower}});
    }
  };
  if(dbQuery.$or.length === 0){
    dbQuery = undefined;
  }
  Playlist.find(dbQuery)
    .skip(skip)
    .limit(config.DEFAULT_QUERY_LIMIT)
    .sort({created_at: -1})
    .populate('creator', '-password') //populate owner field with name and id
    .exec(function(err, playlists){
      if(err){
        console.log(err);
        return res.status(500).send();
      }
      return res.status(200).json(playlists);
    });
}

/**
 * Middleware for storing a search when a GET is made towards /playlists
 */
function storeSearch(req, res, next){
  if(!req.session.user) return next(); //user not logged in
  var search = new Search();
  search.query = req.query.search;
  search.tags = req.query.tag;
  search.user = req.session.user._id;
  search.save(function(err, search){
    if(err) return res.status(500).send();
    User.findOne({_id: req.session.user._id}, function(err, user){
        if(err) return res.status(500).send();
        if(!user) return next();
        if(!user.recentSearches) user.recentSearches = [];
        var recentSearchLength = user.recentSearches.unshift(search._id);
        user.recentSearches = user.recentSearches.splice(0, config.AMOUNT_OF_SEARCHES_TO_STORE-1);
        console.log(user);
        user.save(function(err, user){
          next();
        });
    });
  })
}
