
function validatePlaylistPostRequest(req, res, next){
  if(!req.body.name){
    return res.status(422).json({message: 'Playlist name missing'});
  }
  if(!req.body.spotifyURI){
    return res.status(422).json({message: 'SpotifyURI missing'});
  }
  next();
}

function validatePlaylistGetRequest(req, res, next){
  if(req.query.skip){
      if(!isNumber(parseInt(req.query.skip))) return res.status(400).json({message: 'Skip must be a number'});
  }
  next();
}

module.exports = {
  playlistPost: validatePlaylistPostRequest,
  playlistGet: validatePlaylistGetRequest
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
