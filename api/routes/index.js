var express = require('express');
var router = express.Router();
var usersRouter = require('./users');
var playlistsRouter = require('./playlists');
var loginRouter = require('./login');

module.exports = function () {
	router.get('/', function (req, res) {
		res.send('Api is running');
	});
	router.use('/', usersRouter());
  router.use('/', playlistsRouter());
	router.use('/', loginRouter());
	return router;
};
