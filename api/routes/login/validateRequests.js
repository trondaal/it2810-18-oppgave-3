function validateLoginPostRequest(req, res, next){
  if(!req.body.username){
    return res.status(422).json({message: 'Required field username missing'});
  }
  if(!req.body.password){
    return res.status(422).json({message: 'Required field password missing'});
  }
  next();
}

module.exports = {
  loginPost: validateLoginPostRequest
}
