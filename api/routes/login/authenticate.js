module.exports = {
  normal: function(req, res, next){
    console.log(req.user);
    if(!req.session.user){
      return res.status(401).json({message: 'No session found, please sign in'})
    }
    next();
  }
}
