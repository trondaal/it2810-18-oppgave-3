var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var router = require('./routes');
var cookieParser = require('cookie-parser')
/* Init mongodb/mongoose*/
var dbConfig = require('./config/db.js');
if (mongoose.connection.readyState === 0) {
	mongoose.connect(dbConfig.DATABASE.test, function (err) {
		if (err) console.log(err);
		else console.log('Successfully connected to: ' + dbConfig.DATABASE.test);
	});
};

//Init app
var app = express();
/* Init session */
var sessionConfig = require('./config/session.js');
// CookieParser should be above session
app.use(cookieParser());
// Express MongoDB session storage
app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: sessionConfig.SECRET
}));

// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});;


/* Use body parser */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true, colorize: true}));

/*Cons*/
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://it2810-18.idi.ntnu.no:8080");
    res.header("Access-Control-Allow-Credentials", "true");
		res.header("Access-Control-Allow-Methods: GET, POST, PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* Route /api */
app.use('/api', router());


//Swallow errors
app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

//Listen
var serverConfig = require('./config/server.js');
var port = serverConfig.PORT;
app.listen(port, function() {
	 console.log('Api listening on port ' + port);
});
