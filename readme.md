# Playlstr - A playlist app

## What is this?

Playlstr is an application where users can upload and share spotify-playlists.

## Libraries

- [Angular 2](https://angular.io/)
- [Express](http://expressjs.com/)
- [SystemJS](https://github.com/systemjs/systemjs)
- [mongoose.js](http://mongoosejs.com/)
- [gulp](http://gulpjs.com/)
- [angular2-mdl](https://www.npmjs.com/package/angular2-mdl)
- [angular2-notifications](https://www.npmjs.com/package/angular2-notifications)
- [angular-tag-cloud-module](https://www.npmjs.com/package/angular-tag-cloud-module)

## Documentation

### Architecture

![Arch](https://dl.dropboxusercontent.com/u/41553341/arch2.png)

### Parts

The project is split into two parts:

- [API](./api)
- [Front-end](./webapp)

## Install/Production

Goto [api](./api) or [webapp](./webapp) for more details